# IHD Dataset

State-of-the-art datasets for aerial object detection primarily focus on strong, generic scenarios. However, when it comes to specific aspects like low-light environments, different altitudes, and blurry indoor conditions, these datasets may not suffice for optimal training and detection performance of indoor human detection models. For this reason, we utilize a combination of three existing datasets and our own specially collected dataset. Our dataset has been gathered from various complex public indoor areas, including caves, stadiums, shopping malls, warehouses, and offices. 

For newly collected Areas:
 - Complex public indoor areas
 - Caves
 - Shopping Malls
 - Warehouses
 - Offices
 - Garages
 
UpComming:
 - Museums 
 - Cinema
 - Stadiums
 - ... 

## Note

A sample of our own collected dataset is provided and once the academic paper is published, the entire dataset will be made accessible.

![Alt text](Sample/combined.png)


